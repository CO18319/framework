<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AgadeMail Entity
 *
 * @property int $id
 * @property string|null $title
 * @property \Cake\I18n\FrozenTime|null $date
 * @property string|null $category
 * @property string|null $content
 * @property bool|null $is_public
 *
 * @property \App\Model\Entity\CdliTag[] $cdli_tags
 */
class AgadeMail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'date' => true,
        'category' => true,
        'content' => true,
        'is_public' => true,
        'cdli_tags' => true
    ];
}
