<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AuthorsUpdateEvents Model
 *
 * @property \App\Model\Table\UpdateEventsTable|\Cake\ORM\Association\BelongsTo $UpdateEvents
 * @property \App\Model\Table\AuthorsTable|\Cake\ORM\Association\BelongsTo $Authors
 *
 * @method \App\Model\Entity\AuthorsUpdateEvent get($primaryKey, $options = [])
 * @method \App\Model\Entity\AuthorsUpdateEvent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AuthorsUpdateEvent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AuthorsUpdateEvent|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AuthorsUpdateEvent|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AuthorsUpdateEvent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AuthorsUpdateEvent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AuthorsUpdateEvent findOrCreate($search, callable $callback = null, $options = [])
 */
class AuthorsUpdateEventsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('authors_update_events');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('UpdateEvents', [
            'foreignKey' => 'update_events_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Authors', [
            'foreignKey' => 'author_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['update_events_id'], 'UpdateEvents'));
        $rules->add($rules->existsIn(['author_id'], 'Authors'));

        return $rules;
    }
}
