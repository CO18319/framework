<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Materials Model
 *
 * @property \App\Model\Table\MaterialsTable|\Cake\ORM\Association\BelongsTo $ParentMaterials
 * @property \App\Model\Table\MaterialsTable|\Cake\ORM\Association\HasMany $ChildMaterials
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsToMany $Artifacts
 *
 * @method \App\Model\Entity\Material get($primaryKey, $options = [])
 * @method \App\Model\Entity\Material newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Material[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Material|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Material|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Material patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Material[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Material findOrCreate($search, callable $callback = null, $options = [])
 */
class MaterialsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('materials');
        $this->setDisplayField('material');
        $this->setPrimaryKey('id');

        $this->belongsTo('ParentMaterials', [
            'className' => 'Materials',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildMaterials', [
            'className' => 'Materials',
            'foreignKey' => 'parent_id'
        ]);
        $this->belongsToMany('Artifacts', [
            'foreignKey' => 'material_id',
            'targetForeignKey' => 'artifact_id',
            'joinTable' => 'artifacts_materials'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('material')
            ->maxLength('material', 50)
            ->requirePresence('material', 'create')
            ->notEmpty('material');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['parent_id'], 'ParentMaterials'));

        return $rules;
    }
}
