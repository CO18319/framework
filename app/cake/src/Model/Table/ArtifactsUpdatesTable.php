<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactsUpdates Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\UpdateEventsTable|\Cake\ORM\Association\BelongsTo $UpdateEvents
 *
 * @method \App\Model\Entity\ArtifactsUpdate get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate findOrCreate($search, callable $callback = null, $options = [])
 */
class ArtifactsUpdatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('artifacts_updates');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->belongsTo('UpdateEvents', [
            'foreignKey' => 'update_events_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('comments')
            ->maxLength('comments', 100)
            ->allowEmpty('comments');

        $validator
            ->scalar('designation')
            ->requirePresence('designation', 'create')
            ->notEmpty('designation');

        $validator
            ->scalar('artifact_type')
            ->requirePresence('artifact_type', 'create')
            ->notEmpty('artifact_type');

        $validator
            ->scalar('period')
            ->requirePresence('period', 'create')
            ->notEmpty('period');

        $validator
            ->scalar('provenience')
            ->requirePresence('provenience', 'create')
            ->notEmpty('provenience');

        $validator
            ->scalar('written_in')
            ->requirePresence('written_in', 'create')
            ->notEmpty('written_in');

        $validator
            ->scalar('archive')
            ->requirePresence('archive', 'create')
            ->notEmpty('archive');

        $validator
            ->scalar('composite_no')
            ->maxLength('composite_no', 250)
            ->requirePresence('composite_no', 'create')
            ->notEmpty('composite_no');

        $validator
            ->scalar('seal_no')
            ->maxLength('seal_no', 250)
            ->requirePresence('seal_no', 'create')
            ->notEmpty('seal_no');

        $validator
            ->scalar('composites')
            ->requirePresence('composites', 'create')
            ->notEmpty('composites');

        $validator
            ->scalar('seals')
            ->requirePresence('seals', 'create')
            ->notEmpty('seals');

        $validator
            ->scalar('museum_no')
            ->maxLength('museum_no', 11)
            ->requirePresence('museum_no', 'create')
            ->notEmpty('museum_no');

        $validator
            ->scalar('accession_no')
            ->maxLength('accession_no', 11)
            ->requirePresence('accession_no', 'create')
            ->notEmpty('accession_no');

        $validator
            ->scalar('condition_description')
            ->requirePresence('condition_description', 'create')
            ->notEmpty('condition_description');

        $validator
            ->scalar('artifact_preservation')
            ->requirePresence('artifact_preservation', 'create')
            ->notEmpty('artifact_preservation');

        $validator
            ->scalar('period_comments')
            ->requirePresence('period_comments', 'create')
            ->notEmpty('period_comments');

        $validator
            ->scalar('provenience_comments')
            ->requirePresence('provenience_comments', 'create')
            ->notEmpty('provenience_comments');

        $validator
            ->boolean('is_provenience_uncertain')
            ->requirePresence('is_provenience_uncertain', 'create')
            ->notEmpty('is_provenience_uncertain');

        $validator
            ->boolean('is_period_uncertain')
            ->requirePresence('is_period_uncertain', 'create')
            ->notEmpty('is_period_uncertain');

        $validator
            ->boolean('is_artifact_type_uncertain')
            ->requirePresence('is_artifact_type_uncertain', 'create')
            ->notEmpty('is_artifact_type_uncertain');

        $validator
            ->boolean('is_school_text')
            ->requirePresence('is_school_text', 'create')
            ->notEmpty('is_school_text');

        $validator
            ->decimal('height')
            ->requirePresence('height', 'create')
            ->notEmpty('height');

        $validator
            ->decimal('thickness')
            ->requirePresence('thickness', 'create')
            ->notEmpty('thickness');

        $validator
            ->decimal('width')
            ->requirePresence('width', 'create')
            ->notEmpty('width');

        $validator
            ->decimal('weight')
            ->requirePresence('weight', 'create')
            ->notEmpty('weight');

        $validator
            ->scalar('elevation')
            ->requirePresence('elevation', 'create')
            ->notEmpty('elevation');

        $validator
            ->scalar('excavation_no')
            ->requirePresence('excavation_no', 'create')
            ->notEmpty('excavation_no');

        $validator
            ->scalar('findspot_square')
            ->requirePresence('findspot_square', 'create')
            ->notEmpty('findspot_square');

        $validator
            ->scalar('findspot_comments')
            ->requirePresence('findspot_comments', 'create')
            ->notEmpty('findspot_comments');

        $validator
            ->scalar('stratigraphic_level')
            ->requirePresence('stratigraphic_level', 'create')
            ->notEmpty('stratigraphic_level');

        $validator
            ->scalar('surface_preservation')
            ->requirePresence('surface_preservation', 'create')
            ->notEmpty('surface_preservation');

        $validator
            ->scalar('artifact_comments')
            ->requirePresence('artifact_comments', 'create')
            ->notEmpty('artifact_comments');

        $validator
            ->scalar('seal_information')
            ->requirePresence('seal_information', 'create')
            ->notEmpty('seal_information');

        $validator
            ->boolean('is_public')
            ->requirePresence('is_public', 'create')
            ->notEmpty('is_public');

        $validator
            ->boolean('is_atf_public')
            ->requirePresence('is_atf_public', 'create')
            ->notEmpty('is_atf_public');

        $validator
            ->integer('are_images_public')
            ->requirePresence('are_images_public', 'create')
            ->notEmpty('are_images_public');

        $validator
            ->integer('artifacts_collections')
            ->requirePresence('artifacts_collections', 'create')
            ->notEmpty('artifacts_collections');

        $validator
            ->scalar('artifacts_composites')
            ->requirePresence('artifacts_composites', 'create')
            ->notEmpty('artifacts_composites');

        $validator
            ->scalar('artifacts_seals')
            ->requirePresence('artifacts_seals', 'create')
            ->notEmpty('artifacts_seals');

        $validator
            ->scalar('artifacts_dates')
            ->requirePresence('artifacts_dates', 'create')
            ->notEmpty('artifacts_dates');

        $validator
            ->scalar('alternative_years')
            ->requirePresence('alternative_years', 'create')
            ->notEmpty('alternative_years');

        $validator
            ->scalar('artifacts_genres')
            ->requirePresence('artifacts_genres', 'create')
            ->notEmpty('artifacts_genres');

        $validator
            ->scalar('artifacts_languages')
            ->requirePresence('artifacts_languages', 'create')
            ->notEmpty('artifacts_languages');

        $validator
            ->scalar('artifacts_materials')
            ->requirePresence('artifacts_materials', 'create')
            ->notEmpty('artifacts_materials');

        $validator
            ->scalar('artifacts_shadow_cdli_comments')
            ->requirePresence('artifacts_shadow_cdli_comments', 'create')
            ->notEmpty('artifacts_shadow_cdli_comments');

        $validator
            ->scalar('artifacts_shadow_collection_location')
            ->requirePresence('artifacts_shadow_collection_location', 'create')
            ->notEmpty('artifacts_shadow_collection_location');

        $validator
            ->scalar('artifacts_shadow_collection_comments')
            ->requirePresence('artifacts_shadow_collection_comments', 'create')
            ->notEmpty('artifacts_shadow_collection_comments');

        $validator
            ->scalar('artifacts_shadow_acquisition_history')
            ->requirePresence('artifacts_shadow_acquisition_history', 'create')
            ->notEmpty('artifacts_shadow_acquisition_history');

        $validator
            ->scalar('pubications_key')
            ->requirePresence('pubications_key', 'create')
            ->notEmpty('pubications_key');

        $validator
            ->scalar('publications_type')
            ->requirePresence('publications_type', 'create')
            ->notEmpty('publications_type');

        $validator
            ->scalar('publications_exact_ref')
            ->requirePresence('publications_exact_ref', 'create')
            ->notEmpty('publications_exact_ref');

        $validator
            ->scalar('publications_comment')
            ->requirePresence('publications_comment', 'create')
            ->notEmpty('publications_comment');

        $validator
            ->integer('approved_by')
            ->requirePresence('approved_by', 'create')
            ->notEmpty('approved_by');

        $validator
            ->boolean('approved')
            ->requirePresence('approved', 'create')
            ->notEmpty('approved');

        $validator
            ->scalar('publication_error')
            ->requirePresence('publication_error', 'create')
            ->notEmpty('publication_error');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'));
        $rules->add($rules->existsIn(['update_events_id'], 'UpdateEvents'));

        return $rules;
    }
}
