<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EntryTypes Model
 *
 * @property \App\Model\Table\PublicationsTable|\Cake\ORM\Association\HasMany $Publications
 *
 * @method \App\Model\Entity\EntryType get($primaryKey, $options = [])
 * @method \App\Model\Entity\EntryType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\EntryType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EntryType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EntryType|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EntryType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EntryType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\EntryType findOrCreate($search, callable $callback = null, $options = [])
 */
class EntryTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('entry_types');
        $this->setDisplayField('label');
        $this->setPrimaryKey('id');

        $this->hasMany('Publications', [
            'foreignKey' => 'entry_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('label')
            ->maxLength('label', 25)
            ->allowEmpty('label');

        $validator
            ->scalar('author')
            ->maxLength('author', 3)
            ->allowEmpty('author');

        $validator
            ->scalar('title')
            ->maxLength('title', 3)
            ->allowEmpty('title');

        $validator
            ->scalar('journal')
            ->maxLength('journal', 3)
            ->allowEmpty('journal');

        $validator
            ->scalar('year')
            ->maxLength('year', 3)
            ->allowEmpty('year');

        $validator
            ->scalar('volume')
            ->maxLength('volume', 3)
            ->allowEmpty('volume');

        $validator
            ->scalar('pages')
            ->maxLength('pages', 3)
            ->allowEmpty('pages');

        $validator
            ->scalar('number')
            ->maxLength('number', 3)
            ->allowEmpty('number');

        $validator
            ->scalar('month')
            ->maxLength('month', 3)
            ->allowEmpty('month');

        $validator
            ->scalar('eid')
            ->maxLength('eid', 3)
            ->allowEmpty('eid');

        $validator
            ->scalar('note')
            ->maxLength('note', 3)
            ->allowEmpty('note');

        $validator
            ->scalar('crossref')
            ->maxLength('crossref', 3)
            ->allowEmpty('crossref');

        $validator
            ->scalar('keyword')
            ->maxLength('keyword', 3)
            ->allowEmpty('keyword');

        $validator
            ->scalar('doi')
            ->maxLength('doi', 3)
            ->allowEmpty('doi');

        $validator
            ->scalar('url')
            ->maxLength('url', 3)
            ->allowEmpty('url');

        $validator
            ->scalar('file')
            ->maxLength('file', 3)
            ->allowEmpty('file');

        $validator
            ->scalar('citeseerurl')
            ->maxLength('citeseerurl', 3)
            ->allowEmpty('citeseerurl');

        $validator
            ->scalar('pdf')
            ->maxLength('pdf', 3)
            ->allowEmpty('pdf');

        $validator
            ->scalar('abstract')
            ->maxLength('abstract', 3)
            ->allowEmpty('abstract');

        $validator
            ->scalar('comment')
            ->maxLength('comment', 3)
            ->allowEmpty('comment');

        $validator
            ->scalar('owner')
            ->maxLength('owner', 3)
            ->allowEmpty('owner');

        $validator
            ->scalar('timestamp')
            ->maxLength('timestamp', 3)
            ->allowEmpty('timestamp');

        $validator
            ->scalar('review')
            ->maxLength('review', 3)
            ->allowEmpty('review');

        $validator
            ->scalar('search')
            ->maxLength('search', 3)
            ->allowEmpty('search');

        $validator
            ->scalar('publisher')
            ->maxLength('publisher', 3)
            ->allowEmpty('publisher');

        $validator
            ->scalar('editor')
            ->maxLength('editor', 3)
            ->allowEmpty('editor');

        $validator
            ->scalar('series')
            ->maxLength('series', 3)
            ->allowEmpty('series');

        $validator
            ->scalar('address')
            ->maxLength('address', 3)
            ->allowEmpty('address');

        $validator
            ->scalar('edition')
            ->maxLength('edition', 3)
            ->allowEmpty('edition');

        $validator
            ->scalar('howpublished')
            ->maxLength('howpublished', 3)
            ->allowEmpty('howpublished');

        $validator
            ->scalar('lastchecked')
            ->maxLength('lastchecked', 3)
            ->allowEmpty('lastchecked');

        $validator
            ->scalar('booktitle')
            ->maxLength('booktitle', 3)
            ->allowEmpty('booktitle');

        $validator
            ->scalar('organization')
            ->maxLength('organization', 3)
            ->allowEmpty('organization');

        $validator
            ->scalar('language')
            ->maxLength('language', 3)
            ->allowEmpty('language');

        $validator
            ->scalar('chapter')
            ->maxLength('chapter', 3)
            ->allowEmpty('chapter');

        $validator
            ->scalar('type')
            ->maxLength('type', 3)
            ->allowEmpty('type');

        $validator
            ->scalar('school')
            ->maxLength('school', 3)
            ->allowEmpty('school');

        $validator
            ->scalar('nationality')
            ->maxLength('nationality', 3)
            ->allowEmpty('nationality');

        $validator
            ->scalar('yearfiled')
            ->maxLength('yearfiled', 3)
            ->allowEmpty('yearfiled');

        $validator
            ->scalar('assignee')
            ->maxLength('assignee', 3)
            ->allowEmpty('assignee');

        $validator
            ->scalar('day')
            ->maxLength('day', 3)
            ->allowEmpty('day');

        $validator
            ->scalar('dayfiled')
            ->maxLength('dayfiled', 3)
            ->allowEmpty('dayfiled');

        $validator
            ->scalar('monthfiled')
            ->maxLength('monthfiled', 3)
            ->allowEmpty('monthfiled');

        $validator
            ->scalar('institution')
            ->maxLength('institution', 3)
            ->allowEmpty('institution');

        $validator
            ->scalar('revision')
            ->maxLength('revision', 3)
            ->allowEmpty('revision');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));

        return $rules;
    }
}
