<?php
namespace App\View;

use Cake\View\SerializedView;

class CslJsonView extends SerializedView
{
    use SerializeTrait;

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize'];

    /**
     * @var string
     */
    protected $_responseType = 'csljson';

    public function initialize()
    {
        parent::initialize();
        $this->loadHelper('Scripts');
    }

    protected function _serialize($serialize)
    {
        return $this->Scripts->formatReference(
            $this->_dataToSerialize($serialize),
            'data',
            []
        );
    }
}
