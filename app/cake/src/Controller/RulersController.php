<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Rulers Controller
 *
 * @property \App\Model\Table\RulersTable $Rulers
 *
 * @method \App\Model\Entity\Ruler[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RulersController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('LinkedData');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Periods', 'Dynasties', 'Dates']
        ];
        $rulers = $this->paginate($this->Rulers);

        $this->set(compact('rulers'));
        $this->set('_serialize', 'rulers');
    }

    /**
     * View method
     *
     * @param string|null $id Ruler id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ruler = $this->Rulers->get($id, [
            'contain' => ['Periods', 'Dynasties', 'Dates']
        ]);

        $this->set('ruler', $ruler);
        $this->set('_serialize', 'ruler');
    }
}
