<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Archives Controller
 *
 * @property \App\Model\Table\ArchivesTable $Archives
 *
 * @method \App\Model\Entity\Archive[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArchivesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('LinkedData');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Proveniences']
        ];
        $archives = $this->paginate($this->Archives);

        $this->set(compact('archives'));
        $this->set('_serialize', 'archives');
    }

    /**
     * View method
     *
     * @param string|null $id Archive id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $archive = $this->Archives->get($id, [
            'contain' => ['Proveniences']
        ]);

        $this->set('archive', $archive);
        $this->set('_serialize', 'archive');
    }
}
