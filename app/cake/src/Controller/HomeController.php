<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Home Controller
 *
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomeController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Set access for public.
        $this->Auth->allow(['index', 'browse']);
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Postings');
        $this->set('newsarticles', $this->Postings->find('all', [
              'conditions' => ['Postings.published' => 1, 'posting_type_id' => 1],
              'limit' => 6
            ]));
        $this->set('highlights', $this->Postings->find('all', [
            'conditions' => ['Postings.published' => 1, 'posting_type_id' => 3],
            'limit' => 6
            ]));
    }

    /**
     * Browse method
     *
     * @return \Cake\Http\Response|void
     */
    public function browse()
    {
        $this->loadModel('Artifacts');
        $this->paginate = [
            'contain' => ['Proveniences', 'Periods', 'ArtifactTypes', 'Archives']
        ];
        $artifacts = $this->paginate($this->Artifacts);

        $this->set(compact('artifacts'));
        $this->set('_serialize', ['artifacts']);
    }
}
