<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ArtifactsCollections Controller
 *
 * @property \App\Model\Table\ArtifactsCollectionsTable $ArtifactsCollections
 *
 * @method \App\Model\Entity\ArtifactsCollection[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsCollectionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'Collections']
        ];
        $artifactsCollections = $this->paginate($this->ArtifactsCollections);

        $this->set(compact('artifactsCollections'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Collection id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsCollection = $this->ArtifactsCollections->get($id, [
            'contain' => ['Artifacts', 'Collections']
        ]);

        $this->set('artifactsCollection', $artifactsCollection);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactsCollection = $this->ArtifactsCollections->newEntity();
        if ($this->request->is('post')) {
            $artifactsCollection = $this->ArtifactsCollections->patchEntity($artifactsCollection, $this->request->getData());
            if ($this->ArtifactsCollections->save($artifactsCollection)) {
                $this->Flash->success(__('The artifacts collection has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts collection could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsCollections->Artifacts->find('list', ['limit' => 200]);
        $collections = $this->ArtifactsCollections->Collections->find('list', ['limit' => 200]);
        $this->set(compact('artifactsCollection', 'artifacts', 'collections'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifacts Collection id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactsCollection = $this->ArtifactsCollections->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsCollection = $this->ArtifactsCollections->patchEntity($artifactsCollection, $this->request->getData());
            if ($this->ArtifactsCollections->save($artifactsCollection)) {
                $this->Flash->success(__('The artifacts collection has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts collection could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsCollections->Artifacts->find('list', ['limit' => 200]);
        $collections = $this->ArtifactsCollections->Collections->find('list', ['limit' => 200]);
        $this->set(compact('artifactsCollection', 'artifacts', 'collections'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifacts Collection id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsCollection = $this->ArtifactsCollections->get($id);
        if ($this->ArtifactsCollections->delete($artifactsCollection)) {
            $this->Flash->success(__('The artifacts collection has been deleted.'));
        } else {
            $this->Flash->error(__('The artifacts collection could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
