<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication $publication
 */
?>
<h1 class="display-3 header-txt text-left"><?= __('View Publication Details') ?></h1>
<div class="row justify-content-md-center">
    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Publication') ?>
        <?php if ($is_admin): ?>
            <?= $this->Html->link('Edit Publication', ['prefix' => 'admin', 'action' => 'edit', $publication->id], ['class' => 'btn btn-action', 'style' => "float: right;"]) ?>
        <?php endif ?>
        </div>
        <table class="table-bootstrap mx-0">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Designation') ?></th>
                    <td>
                        <?= h($publication->designation) ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Reference') ?></th>
                    <td>
                        <?= $this->Scripts->formatReference($publication, 'bibliography', [
                            'template' => 'chicago-author-date',
                            'format' => 'html'
                        ]) ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Authors') ?></th>
                    <td>
                        <?php foreach ($publication->authors as $author): ?>
                            <?= $this->Html->link(
                                h($author->author),
                                ['controller' => 'Authors', 'action' => 'view', $author->id]) ?><br>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Editors') ?></th>
                    <td>
                        <?php foreach ($publication->editors as $editor): ?>
                            <?= $this->Html->link(
                                h($editor->author),
                                ['controller' => 'Authors', 'action' => 'view', $editor->id]) ?><br>
                        <?php endforeach; ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="boxed mx-0">
    <?php if (empty($publication->artifacts)): ?>
    <div class="capital-heading"><?= __('No Related Artifacts') ?>
    <?php if ($is_admin): ?>
        <?= $this->Html->link('Link Artifacts', [
                    'prefix' => 'admin',
                    'controller' => 'ArtifactsPublications',
                    'action' => 'add', 'artifact', $publication->id
                    ],
                    ['class' => 'btn btn-action', 'style' => "float: right;"]) ?>
    <?php endif ?>
    </div>
    <?php else: ?>
    <div class="capital-heading"><?= __('Related Artifacts') ?>
    <?php if ($is_admin): ?>
        <?= $this->Html->link('Link Artifacts', [
                    'prefix' => 'admin',
                    'controller' => 'ArtifactsPublications',
                    'action' => 'add', 'artifact', $publication->id
                    ],
                    ['class' => 'btn btn-action', 'style' => "float: right;"]) ?>
    <?php endif ?>
    </div>
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table-bootstrap mx-0">
            <thead>
                <th scope="col"><?= __('Artifact Identifier') ?></th>
                <th scope="col"><?= __('Artifact Designation') ?></th>
                <th scope="col"><?= __('Musuem Collections') ?></th>
                <th scope="col"><?= __('Period') ?></th>
                <th scope="col"><?= __('Provenience') ?></th>
                <th scope="col"><?= __('Artifact Type') ?></th>
                <th scope="col"><?= __('Publication Type') ?></th>
                <th scope="col"><?= __('Exact Reference') ?></th>
                <th scope="col"><?= __('Publication Comments') ?></th>
            </thead>
            <tbody>
                <?php foreach ($publication->artifacts as $artifact): ?>
                <tr>
                    <td><?= $this->Html->link(h('P'.substr("00000{$artifact->id}", -6)), ['controller' => 'Artifacts', 'action' => 'view', $artifact->id]) ?>
                    </td>
                    <td><?= h($artifact->designation) ?></td>
                    <td>
                        <?php foreach ($artifact->collections as $collection): ?>
                        <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id]) ?>
                        <?php endforeach; ?>
                    </td>
                    <td>
                        <?php if (!empty($artifact->period)): ?>
                        <?= $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) ?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if (!empty($artifact->provenience)): ?>
                        <?= $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) ?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if (!empty($artifact->artifact_type)): ?>
                        <?= $this->Html->link($artifact->artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) ?>
                        <?php endif; ?>
                    </td>
                    <td><?= h($artifact->_joinData->publication_type) ?></td>
                    <td><?= h($artifact->_joinData->exact_reference) ?></td>
                    <td><?= h($artifact->publication_comments) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php endif; ?>
</div>
