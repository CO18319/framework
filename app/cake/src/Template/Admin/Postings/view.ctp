<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Posting $posting
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Posting') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Posting Type') ?></th>
                    <td><?= $posting->has('posting_type') ? $this->Html->link($posting->posting_type->id, ['controller' => 'PostingTypes', 'action' => 'view', $posting->posting_type->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Slug') ?></th>
                    <td><?= h($posting->slug) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Lang') ?></th>
                    <td><?= h($posting->lang) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified By') ?></th>
                    <td><?= h($posting->modified_by) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($posting->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created By') ?></th>
                    <td><?= $this->Number->format($posting->created_by) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Created') ?></th>
                    <td><?= h($posting->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Modified') ?></th>
                    <td><?= h($posting->modified) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Publish Start') ?></th>
                    <td><?= h($posting->publish_start) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Publish End') ?></th>
                    <td><?= h($posting->publish_end) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Published') ?></th>
                    <td><?= $posting->published ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Title') ?></th>
                    <td><?= $this->Text->autoParagraph(h($posting->title)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Body') ?></th>
                    <td><?= $this->Text->autoParagraph(h($posting->body)); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Posting'), ['action' => 'edit', $posting->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Posting'), ['action' => 'delete', $posting->id], ['confirm' => __('Are you sure you want to delete # {0}?', $posting->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Postings'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Posting'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Posting Types'), ['controller' => 'PostingTypes', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Posting Type'), ['controller' => 'PostingTypes', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>



