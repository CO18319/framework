<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal $journal
 */
?>

<div class="row justify-content-md-center">
    <div id="articleEditDiv" class="col-lg-7 boxed">
        <h4>Add Cuneiform Digital Library Note</h4>
        <hr>
        <?= $this->Form->create('',array('id' => 'form-cdln')) ?>
            <div class="form-group">
                <label>Article Title</label>
                <input type="text" name="cdln_title" class="article_title_input_lg form-control" placeholder="">
                <small class="form-text text-muted">This will be displayed as page headers.</small>
            </div>
            <div class="form-group">
                <label>Serial No.</label>
                <input type="text" name="serial" value="2020:" class="article_title_input_lg form-control" placeholder="">
                <small class="form-text text-muted" >This will be displayed as link parameter.</small>
            </div>
            <div class="form-group">
                <label for="">Author(s)</label>
                <input id="article_author_input" name="cdln_authors" type="text" class="article_title_input_lg form-control">
                <div id="input-foot-tags-parent" class="input-foot-tags-parent">
                </div>
            </div>
            <div class="form-group">
                <label for="">Note</label>
                <textarea style="opacity:0" name="cdln_editor" id="cdln_editor" cols="30" rows="10"></textarea>
                <textarea style="display:none" id="cdln_content" name="cdln_content"></textarea>
            </div>
            <div class="form-group">
                <label for="">Status</label>
                <select class="form-control" id="" name="cdln_status">
                    <option value="0">Created</option>
                    <option value="1">Under Review</option>
                    <option value="2">Accepted</option>
                    <option value="3">Published</option>
                    <option value="4">Unpublish</option>
                </select>
                <small class="form-text text-muted">This article will be assigned to the logged in user.</small>
            </div>
            <div id="cdln_error" style="display:none;" class="alert alert-danger" role="alert">
                Please fill all the required details.
            </div>
            <button id="showPreviewButton" type="button" onclick="toggle_cdln_preview('show')" class="btn btn-primary">Preview</button>
            <button type="button" onclick="submit_cdln()" class="btn btn-primary">Save</button>
            
        <?= $this->Form->end() ?>
        <button id="closePreviewButton" style="display:none" type="button" onclick="toggle_cdln_preview('hide')" class="btn btn-primary">
            Close Preview <i class="fa fa-times" aria-hidden="true"></i>
        </button>
    </div>
    <div class="col-lg boxed" id="articlePreviewDiv" style="display:none">
        <div class="capital-heading"><?= __('Preview') ?></div>
        <div id="myProgress">
            <div id="myBar"></div>
        </div>
        <hr>
        <div class="preview" style="height:80vh;overflow-x:scroll;overflow-y:scroll;background-color:white;padding:5%">
            <table width="750" border="0" cellpadding="0" cellspacing="5">
                <tr>
                    <td valign="top" width="400"> 
                    <p style="font-family: S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif; font-size:9pt">
                        Cuneiform Digital Library Journal <br>
                        <b> 2020:xx </b> 
                        <br>
                        <font size="1"> ISSN 1540-8779 <br>
                        &#169; <i> Cuneiform Digital Library Initiative </i> </font> &nbsp; 
                    </p>
                    </td>
                    <td width="200" height="200" rowspan="2" align="right" nowrap bgcolor="#1461ab"> 
                    <p style="line-height:15.0pt; font-size:9pt;color:white;">
                        <font face="S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif"> <a href="/"> CDLI Home </a> <br>
                        <a href=""> CDLI Publications </a> <br>
                        <a href="" target="link"> Editorial Notes </a> <br>
                        <a href="" target="link"> Abbreviations </a> <br>
                        <a href="" target="link"> Bibliography </a> <br>
                        <br>
                        <a href="" target="blank">PDF Version of this Article </a> 
                        <br>
                        <a href="" target="link"> Get Acrobat Reader </a> <br>
                        <a href="" target="link"> <font color="#800517"><b>Download Cuneiform Font</b></font> </a> </font> 
                    </p>
                    </td>
                    <td width="1" rowspan="2" align="right" nowrap bgcolor="#99CCCC"> &nbsp; </td>
                </tr>
                <tr>
                    <td> 
                    <p>
                        <font face="S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif"> 
                        <h2 style="font-family: S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif">
                        <br>
                        <p id="pArticleName"></p>
                        </h2>
                        <b> <p id="pArticleAuthors"> Name </p</b> 
                        <br>
                        <i>University</i> 
                        <br>
                        <br>
                        <b> Keywords </b> <br>
                        </font> 
                    </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center"> 
                    <hr align="center" width="600" size="2">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="left"> 
                    <br>
                    <p>
                    <i><b>Abstract</b><br><br></i>
                    </p>
                    <div id="pArticleContent"></div>
                    
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="relatedActions" class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
    </div>
</div> 
<!-- page script for admin/journals/add_cdln -->
<script type="text/javascript" async
  src="/assets/js/mathjax/MathJax.js?config=TeX-AMS_HTML">
</script>
<script src="/assets/js/ckeditor/ckeditor.js"></script>
<script src="/assets/js/jquery.uploadfile.min.js"></script>
<script>
 CKEDITOR.replace('cdln_editor', {
      extraPlugins: 'mathjax',
      mathJaxLib: '/assets/js/mathjax/MathJax.js?config=TeX-AMS_HTML',
      height: 120
});
</script>
<script src="/assets/js/journals_dashboard.js"></script>
