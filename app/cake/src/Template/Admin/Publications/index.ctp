<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication[]|\Cake\Collection\CollectionInterface $publications
 */
?>

<h1 class="display-3 header-txt text-left"><?= __('Publications Index') ?></h1>
<?= $this->Form->create('Publications', ['action' => 'index', 'type' => 'get']) ?>

<div class="mx-0 boxed">
    <div class="col-lg ads row justify-content-md-center">
        <legend class="capital-heading"><?= __('Publication Search') ?></legend>
        <div class="layout-grid text-left">
            <div>
                BibTeX Key:
                <?= $this->Form->input('bibtexkey', ['label' => false , 'type' => 'text', 'value' => isset($data['bibtexkey']) ? $data['bibtexkey']:null]) ?>
                Title:
                <?= $this->Form->input('title', ['label' => false, 'type' => 'text', 'value' => isset($data['title']) ? $data['title']:null]) ?>
                Publisher:
                <?= $this->Form->input('publisher', ['label' => false, 'type' => 'text', 'value' => isset($data['publisher']) ? $data['publisher']:null]) ?>
                Author:
                <?= $this->Form->input('author', ['label' => false, 'type' => 'text', 'value' => isset($data['author']) ? $data['author']:null]) ?>
                Designation:
                <?= $this->Form->input('designation', ['label' => false, 'type' => 'text', 'value' => isset($data['designation']) ? $data['designation']:null]) ?>
            </div>
            <div>
                Artifact:
                <?= $this->Form->input('artifact', ['label' => false, 'type' => 'text', 'value' => isset($data['artifact']) ? $data['artifact']:null]) ?>
                Entry Type:
                <?= $this->Form->control('entry_type_id', ['label' => false, 'options' => $entryTypes, 'empty' => true, 'value' => isset($data['entry_type_id']) ? $data['entry_type_id']:null, 'class' => 'form-select']);  ?>
                Journal:
                <?= $this->Form->control('journal_id', ['label' => false, 'options' => $journals, 'empty' => true, 'value' => isset($data['journal_id']) ? $data['journal_id']:null, 'class' => 'form-select']);  ?>
                Year:
                <div class="layout-grid text-left">
                    <div>
                    From:<?= $this->Form->control('from', ['label' => false, 'type' => 'number', 'value' => isset($data['from']) ? $data['from']:null]) ?>
                    </div>
                    <div>
                    To:<?= $this->Form->control('to', ['label' => false, 'type' => 'number', 'value' => isset($data['to']) ? $data['to']:null]) ?>
                    </div>
                </div>
            </div>
        </div>
        <table>
            <tr>
                <td>
                    <?= $this->Form->submit('Search', ['class' => 'btn cdli-btn-blue']) ?>
                </td>
                <td>
                    <a href="/admin/publications">
                        <button class='btn' type='button'>Reset</button>
                    </a>
                </td>
            </tr>
        </table>
    </div>
</div>
<?= $this->Form->end() ?>

<?php if ((!isset($publications)) or ($publications->count() == 0)): ?>
    <legend><?= __('No Search Results') ?></legend>
<?php else: ?>
    <div class="table-responsive">
        <table class="table-bootstrap mx-0">
            <thead>
                <tr>
                    <th scope="col">BibTeX key</th>
                    <th scope="col">Authors</th>
                    <th scope="col">Title</th>
                    <th scope="col">Journal</th>
                    <th scope="col">Designation</th>
                    <th scope="col">Reference</th>
                    <th scope="col"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($publications as $publication): ?>
                <tr>
                    <td>
                        <?= $this->Html->link(
                                        h($publication->bibtexkey),
                                        ['prefix' => false, 'action' => 'view', $publication->id]) ?>
                    </td>
                    <td align="left" nowrap="nowrap">
                        <?php foreach ($publication->authors as $author): ?>
                        <?= $this->Html->link(
                                        h($author->author),
                                        ['prefix' => false, 'controller' => 'Authors', 'action' => 'view', $author->id]) ?><br>
                        <?php endforeach ?>
                    </td>
                    <td align="left">
                        <?= (isset($publication->title) and !empty($publication->title)) ? '<b>Title: </b>'.h($publication->title) : '' ?><br>
                        <?= (isset($publication->book_title) and !empty($publication->book_title)) ? '<b>Book Title: </b>'.h($publication->book_title) : '' ?><br>
                        <?= (isset($publication->chapter) and !empty($publication->chapter)) ? '<b>Chapter: </b>'.h($publication->chapter) : '' ?>
                    </td>
                    <td align="left">
                        <?= isset($publication->journal->journal) ? '<b>Journal: </b>'.h($publication->journal->journal) : '' ?><br>
                        <?= (isset($publication->volume) and !empty($publication->volume)) ? '<b>Volume: </b>'.h($publication->book_title) : '' ?><br>
                        <?= (isset($publication->number) and !empty($publication->number)) ? '<b>Number: </b>'.h($publication->chapter) : '' ?>
                    </td>
                    <td><?= h($publication->designation) ?></td>
                    <td>
                        <?= $this->Scripts->formatReference($publication, 'bibliography', [
                                'template' => 'chicago-author-date',
                                'format' => 'html'
                            ]) ?>
                    </td>
                    <td>
                        <?= $this->Html->link(
                                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                                        ['action' => 'edit', $publication->id],
                                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                                        ['action' => 'delete', $publication->id],
                                        ['confirm' => __('Are you sure you want to delete # {0}?', $publication->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div>
        <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
            <?= $this->Paginator->first() ?>
            <?= $this->Paginator->prev() ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next() ?>
            <?= $this->Paginator->last() ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?>
        </p>
    </div>
<?php endif ?>
