<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsLanguage $artifactsLanguage
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Artifacts Language') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsLanguage->has('artifact') ? $this->Html->link($artifactsLanguage->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsLanguage->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Language') ?></th>
                    <td><?= $artifactsLanguage->has('language') ? $this->Html->link($artifactsLanguage->language->id, ['controller' => 'Languages', 'action' => 'view', $artifactsLanguage->language->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsLanguage->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Is Language Uncertain') ?></th>
                    <td><?= $artifactsLanguage->is_language_uncertain ? __('Yes') : __('No'); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <!-- <?= $this->Html->link(__('Edit Artifacts Language'), ['action' => 'edit', $artifactsLanguage->id], ['class' => 'btn-action']) ?> -->
        <!-- <?= $this->Form->postLink(__('Delete Artifacts Language'), ['action' => 'delete', $artifactsLanguage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsLanguage->id), 'class' => 'btn-action']) ?> -->
        <?= $this->Html->link(__('List Artifacts Languages'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Artifacts Language'), ['action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
    </div>

</div>



