
<main>
    <h1 class="display-3 text-left header-txt d-none d-lg-block mb-5">TITLE</h1>
    <div>
        <div class="row">
            <div class="col-sm-12 col-lg-8 imageView-area px-0">
                <h2 class="d-lg-none mt-4 text-left text-white">Title for tablet</h2>
                <p class="d-lg-none text-left text-white">Tablet image</p>
                <div class="padded-container">
                    <figure>
                        <img 
                        class="d-block" 
                        src="https://images.unsplash.com/photo-1533035353720-f1c6a75cd8ab?ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80"
                        alt=""
                        />
                    </figure>
                    
                    <a 
                    href="#" 
                    class="d-block text-white text-right mt-5" 
                    download>
                        Download Image
                    </a>
                </div>

            </div>
            <div class="col-sm-12 col-lg-4 image-info text-left px-5">
                <h2 class="mt-5">Transileration and Translation</h2>
                <span>
                    <p class="mb-0">obverse</p>
                    <p> 
                        1. 2(N45) 5(N14)# , SZE~a
                        en: 150 N1 units of barley,
                        2.a. , |GISZ.TE|# DU ME~a
                        en: for ...,
                        2.b. , SZUBUR E2~a# SANGA~a#
                        en: pigsty accountant;
                    </p>
                </span>
                <span>
                    <p class="mb-0">reverse</p>
                    <p> 
                        1. 1(N50) , GAN2
                        en: 10 bur3 field.
                        calculation: 150N1 ÷ 10 = 15N1 barley per bur3
                        following later tradition and assuming this is a seeding account, it would support an identification of the base unit in grain metrology, N1, with ca. 20-25 liters
                    </p>
                </span>
            </div> 
        </div>
    </div>

    <?=$this->Scroll->toTop()?>
</main>
