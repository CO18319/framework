import os
import os
import sys
#
# This files runs the backend test which currently include only phpunit tests
# container dev_cake_composer for the windows user, if you are not a windows
# user please use ./cdlitest at root.
# only supports composer installation and bin/bash.

def run_unit_tests():
    # This function runs the commands to start unit tests for app/cake inside the docker.
    print("Started running phpunit tests...")

    command_drun_unit_tests = os.popen("docker exec -i dev_cake_composer sh -c 'cd srv/app/cake; vendor/bin/phpunit'")
    command_drun_unit_tests = command_drun_unit_tests.read().split('\n')
    print(command_drun_unit_tests)


if __name__ == "__main__":
   run_unit_tests()